<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Update Box</title>
</head>
<body>
<?php
$connection = mysqli_connect("localhost","root","mypass");
$db = mysqli_select_db($connection, 'task');

$id = $_POST['id'];

$query = "SELECT * FROM box WHERE id='$id' ";
$query_run = mysqli_query($connection, $query);

if($query_run)
{
    while($row = mysqli_fetch_array($query_run))
    {
        ?>
        <div class="container">
            <div class="jumbotron">
                <div class="row">
                    <div class="col-md-12">

                        <h2> Edit Box values</h2>
                        <hr>
                        <form action="" method="post">
                            <input type="hidden" name="id" value="<?php echo $row['id'] ?>">
                            <div class="form-group">
                                <label for=""> Name </label>
                                <input type="text" name="name" class="form-control" value="<?php echo $row['name'] ?>" placeholder="Enter First Name" required>
                            </div>
                            <div class="form-group">
                                <label for=""> Length </label>
                                <input type="text" name="length" class="form-control"  value="<?php echo $row['length'] ?>" placeholder="Enter a Length" required  id="box1" oninput="calculate();">
                            </div>
                            <div class="form-group">
                                <label for="">Width </label>
                                <input type="text" name="width" class="form-control"  value="<?php echo $row['width'] ?>" placeholder="Enter width" required id="box2" oninput="calculate();">
                            </div>
                            <div class="form-group">
                                <label for=""> Height </label>
                                <input type="text" name="height" class="form-control"  value="<?php echo $row['height'] ?>" placeholder="Enter Height" required id="box3" oninput="calculate();">
                            </div>
                            <div class="form-group">
                                <label for=""> Box volume </label>
                                <input type="text" name="volume" class="form-control" placeholder="Dont enter" required id="result" value="<?php echo $row['height'] ?>"  >
                            </div>
                            <button type="submit" name="update" class="btn btn-primary"> Update Data </button>

                            <a href="index.php" class="btn btn-danger"> CANCEL </a>
                        </form>

                    </div>
                </div>

                <?php
                if(isset($_POST['update']))
                {
                    $where="where id=".$id;

                    $query=mysqli_query($connection,"update box set name='".$_POST['name']."',  length='".$_POST['length']."',width='".$_POST['width']."' ,height='".$_POST['height']."',volume='".$_POST['volume']."'".$where);
                    if($query)
                    {
                        echo '<script> alert("Data Updated"); </script>';
                        header("location:index.php");
                    }
                    else
                    {
                        echo '<script> alert("Data Not Updated"); </script>';
                    }
                }
                ?>

            </div>
        </div>
        <?php
    }
}
else
{
    // echo '<script> alert("No Record Found"); </script>';
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4>No Record Found</h4>
            </div>
        </div>
    </div>
    <?php
}
?>
</body>
<script type="text/javascript">
    function calculate() {
        var myBox1 = document.getElementById('box1').value;
        var myBox2 = document.getElementById('box2').value;
        var myBox3 = document.getElementById('box3').value;
        var result = document.getElementById('result');
        var myResult = myBox1 * myBox2 *myBox3;
        document.getElementById('result').value = myResult;

    }
</script>
</html>
