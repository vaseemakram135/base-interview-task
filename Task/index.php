
<?php
$connection = mysqli_connect("localhost","root","mypass");
$db = mysqli_select_db($connection, 'task');

//insert query
if(isset($_POST['insert']))
{
    $query = mysqli_query($connection, "insert into box(name,length,width,height,volume)values
          ('" . $_POST['name'] . "','" . $_POST['length'] . "','" . $_POST['width'] . " ','" . $_POST['height'] . "','" . $_POST['volume'] . "')");

    if($query)
    {
        echo '<script> alert("Data Saved"); </script>';
    }
    else
    {
        echo '<script> alert("Data Not Saved"); </script>';
    }
}

// Select the data

$query = "SELECT * FROM box";
$query_run = mysqli_query($connection, $query);

?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>Task</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>
<div class="container">

    <h2 class="text-capitalize text-primary text-center">Area of Boxes</h2>
<br>
<!--    Add Model-->
<div class="container">
    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
     Add Box
    </button>
</div>
    <br><br>  <br><br>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">S.No</th>
            <th scope="col">Name</th>
            <th scope="col">Width</th>
            <th scope="col">Length</th>
            <th scope="col">Height</th>
            <th scope="col">Volume</th>

            <th colspan="2" class="text-center">Actions</th>

        </tr>
        </thead>
        <tbody>
        <?php
        if($query_run)
        {
            while($row = mysqli_fetch_array($query_run))
            {
                ?>
                <tr>
                    <td> <?php echo $row['id']; ?> </td>
                    <td> <?php echo $row['name']; ?> </td>
                    <td> <?php echo $row['length']; ?> </td>
                    <td> <?php echo $row['width']; ?> </td>
                    <td> <?php echo $row['height']; ?> </td>
                    <td> <?php echo $row['volume']; ?> </td>
                    <th>
                        <form action="show.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $row['id'] ?>">
                            <input type="submit" name="Show" class="btn btn-info " value="Show">
                        </form>
                    </th>
                <th>
                        <form action="edit.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $row['id'] ?>">
                            <input type="submit" name="edit" class="btn btn-success" value="EDIT">
                        </form>
                    </th>

                </tr>
                <?php
            }
        }
        else
        {
        ?>
        <tr>
            <td colspan="6"> No Record Found </td>
            </td>
            <?php
            }
            ?>
        </tr>
        </tbody>
    </table>


<!--    Add Model-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Box</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="#" method="post">
                        <div class="form-group">
                            <label for=""> Name </label>
                            <input type="text" name="name" class="form-control" placeholder="Enter Box Name" required>
                        </div>
                        <div class="form-group">
                            <label for=""> Length </label>
                            <input type="text" name="length" class="form-control" placeholder="Enter a Length" required  id="box1" oninput="calculate();">
                        </div>
                        <div class="form-group">
                            <label for="">Width </label>
                            <input type="text" name="width" class="form-control" placeholder="Enter width" required id="box2" oninput="calculate();">
                        </div>
                        <div class="form-group">
                            <label for=""> Height </label>
                            <input type="text" name="height" class="form-control" placeholder="Enter Height" required id="box3" oninput="calculate();">
                        </div>
                        <div class="form-group">
                            <label for=""> Box volume </label>
                            <input type="text" name="volume" class="form-control" placeholder="Dont enter" required id="result" >
                        </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="insert" class="btn btn-primary"> Save Data </button>
                </div>
                </form>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
<!--    add  box model-->
$('#exampleModal').on('shown.bs.modal', function () {
    $('#myInput').trigger('focus')
});
function calculate() {
    var myBox1 = document.getElementById('box1').value;
    var myBox2 = document.getElementById('box2').value;
    var myBox3 = document.getElementById('box3').value;
    var result = document.getElementById('result');
    var myResult = myBox1 * myBox2 *myBox3;
    document.getElementById('result').value = myResult;

}
</script>
</html>